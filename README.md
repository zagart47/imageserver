![GitHub repo file count](https://img.shields.io/github/directory-file-count/zagart47/imageserver)
![GitHub go.mod Go version](https://img.shields.io/github/go-mod/go-version/zagart47/imageserver)
![GitHub last commit](https://img.shields.io/github/last-commit/zagart47/imageserver)
# Test Task solution.

Test task.

## Installation

Use the git cli to clone repository.

```bash
git clone https://github.com/zagart47/imageserver.git
```

## Usage
### Start server
```bash
cd imageserver
go run api/server.go
```

# Attention! You need use the client to work with this server!!!

## Client here >>>>>[https://github.com/zagart47/imageclient](https://github.com/zagart47/imageclient)<<<<<


